"""
Scipy version > 0.18 is needed, due to 'mode' option from scipy.misc.imread function
"""

import os
import glob
import random
import matplotlib.pyplot as plt

from PIL import Image  # for loading images as YCbCr format
import scipy.misc
import scipy.ndimage
import numpy as np
import matplotlib.pyplot as plt

from read_nc import get_data
from write_nc import writenc3D
import tensorflow as tf
import configparser


def get_names(config):
    return os.listdir(config.input)


def is_zero(l):

    # Threshold setting.
    # 过滤掉降水量小于10的数据
    # 在一张几百x几百的大图中,只要存在大于10的数据, 这张图就保留
    for i in l:
        if max(i) > 10:
            return True
    return False


def preprocess_reconstruct(config):
    """test
    如果传入的图是5KM的, 返回是[number, 681, 1081, 2](2.5km)

    是config.input文件夹下所有的nc文件. 比如有10个nc文件, 每个nc文件16个时次
    那么, number = 10*16

    对应三维:
        是读取config.input文件件下的所有nc文件。比如有10个，那么input_ = (41*10, w, j)。
        若考虑地面， input_ = （(41+1)*10, w, j）

    """
    input_, label_ = get_data(config.input)
    feature_ = get_data(config.feature)
    dex = np.isnan(feature_)
    feature_[dex] = 0

    flag = None  # 用于标识是humidity/temperature（不可负） 还是wind（可以负）
    if 'humidity' in config.label:
        feature_ = feature_ / 75.
        flag = True
    elif 'temperature' in config.label:
        feature_ = feature_ / 12.
        flag = True
    else:
        # wind.
        feature_ = feature_ / 3000.
        flag = False

    dex = np.isnan(input_)
    input_[dex] = 0
    #img_ = get_data(config.input)
    # 这一步确保即便.nc文件有问题, 还是按照既定的大小处理.
    # 理论上来说, scale0w==1, scale0j==1
    scale0w = config.input_size_w / input_.shape[-3]
    scale0j = config.input_size_j / input_.shape[-2]
    img_ = scipy.ndimage.zoom(input_, (1, scale0w, scale0j, 1))
    # resize the input data and feature data from input_size to image_size
    scalew = config.image_size_w / config.input_size_w
    scalej = config.image_size_j / config.input_size_j
    # 插值, 图尺寸变大(分辨率变高), 得到的是模糊的图
    # 1)插值得到681*1081 (2.5km) 粗糙数据.(对应doc)
    img_ = scipy.ndimage.interpolation.zoom(img_, (1, scalew, scalej, 1))
    #img_ = np.maximum(img_,0)
    if flag:
        img_ = np.where(img_ < 0, 0, img_)
    img_ = np.where(img_ > 9999, 0, img_)

    # 对于dem, 直接下采样到2.5KM
    scale1w = config.image_size_w / feature_.shape[-2]
    scale1j = config.image_size_j / feature_.shape[-1]
    feature_ = scipy.ndimage.zoom(feature_, (1, scale1w, scale1j))

    shap = img_.shape
    # 双通道的image_
    image_ = np.zeros((shap[0], shap[1], shap[2], shap[3] + 1))
    image_[:, :, :, 0:shap[3]] = img_
    image_[:, :, :, shap[3]] = feature_
    return image_


def preprocess(config):
    '''
    prepare the train data, train data and label data are included.
    函数解析：  从高分辨率的label数据中 得到input_ 以及dem数据feature_
    label_是input_ 直接赋值的。
    '''
    input_, label_ = get_data(config.label)  # 这个和test的时候不同, 这里是label, 上面是input
    feature_ = get_data(config.feature)  # 地形dem
    dex = np.isnan(feature_)
    feature_[dex] = 0

    flag = None  # 用于标识是humidity/temperature（不可负） 还是wind（可以负）
    if 'humidity' in config.label:
        feature_ = feature_ / 75.
        flag = True
    elif 'temperature' in config.label:
        feature_ = feature_ / 12.
        flag = True
    else:
        # wind.
        feature_ = feature_ / 3000.
        flag = False

    dex = np.isnan(input_)
    input_[dex] = 0
    dex = np.isnan(label_)
    label_[dex] = 0

    # filter the data
    print("before filter input size is:")
    # nc 中是多少， 取出来就是多少。 nc (41, 421, 481), shape 就是这个, 如果考虑地面，（42, 421, 481)
    print(input_.shape)

    # 不是降水， 不需要过滤了。
    # input_ = np.array(list(filter(is_zero, input_)))
    # print("after filter the input size is:")
    # print(input_.shape)

    print("after filter the label size is:")
    print(label_.shape)

    # 这里的input_.shape 是 3KM的, 而config.input_size_w 是5KM的,(都是对应doc举例说明的)
    # 所以这一步是1KM的高分辨率数据下采样到5KM.
    scale0w = config.input_size_w / input_.shape[-3]
    scale0j = config.input_size_j / input_.shape[-2]
    img_ = scipy.ndimage.zoom(input_, (1, scale0w, scale0j, 1))
    # resize the input data and feature data from input_size to image_size
    scalew = config.image_size_w / config.input_size_w
    scalej = config.image_size_j / config.input_size_j
    # 插值, 得到粗糙的数据(2.5KM) 也就是doc里的 'A'
    img_ = scipy.ndimage.interpolation.zoom(img_, (1, scalew, scalej, 1))
    #img_ = np.maximum(img_,0)
    if flag:
        img_ = np.where(img_ < 0, 0, img_)
    img_ = np.where(img_ > 9999, 0, img_)

    # dem, 直接下采样 到2.5KM
    scale1w = config.image_size_w / feature_.shape[-2]
    scale1j = config.image_size_j / feature_.shape[-1]
    feature_ = scipy.ndimage.zoom(feature_, (1, scale1w, scale1j))

    # 对于较为精确的数据'B', 直接下采样的2.5KM
    scale2w = config.label_size_w / label_.shape[-3]
    scale2j = config.label_size_j / label_.shape[-2]
    label_ = scipy.ndimage.zoom(label_, (1, scale2w, scale2j, 1))
    if flag:
        label_ = np.where(label_ < 0, 0, label_)

    shap = img_.shape
    image_ = np.zeros((shap[0], shap[1], shap[2], shap[3] + 1))
    image_[:, :, :, 0:shap[3]] = img_
    image_[:, :, :, shap[3]] = feature_

    # image_.shape=(-1, w, j, 43)
    # label_.shape=(-1, w, j, 41)
    sess = tf.Session()
    sess.run(tf.global_variables_initializer())
    # standardization
    standImage = []
    standLabel = []
    for img in image_:
        tf_img = tf.image.per_image_standardization(img)
        standImage.append(tf_img.eval(session=sess))
    for label in label_:
        tf_label = tf.image.per_image_standardization(label)
        standLabel.append(tf_label.eval(session=sess))
    standImage = np.asarray(standImage)
    standLabel = np.asarray(standLabel)
    print('After standardization, the Image shape=', standImage.shape)
    print('After standardization, the Label shape=', standLabel.shape)
    sess.close()
    return standImage, standLabel


def input_setup(config):
    """传入FLAGS, 根据is_train, 输出

    根据config.patch_size 返回的
    train_data.shape = (?, patch_size, patch_size, 43)
    train_label.shape = (?, patch_size_l, patch_size_l, 41)
    """
    sub_input_sequence = []
    sub_label_sequence = []
    nxy = []
    if config.is_train:

        input, label = preprocess(config)  # input.shape=(-1, w, j, 43)
        shape_i = input.shape
        shape_l = label.shape

        print("image size:")
        print(shape_i)
        print("label size:")
        print(shape_l)
        patch_size = config.patch_size
        patch_size_l = config.patch_size_l
        padding = abs(patch_size - patch_size_l) // 2

        # 先按照padding后的大小创建大图
        input_ = np.zeros((shape_i[0], shape_i[1] + 2 * padding + config.stride,
                           shape_i[2] + 2 * padding + config.stride, shape_i[3]))  # shape=(-1, w', j', 43)  w'>w, j'>j
        label_ = np.zeros((shape_l[0], shape_l[1] + 2 * padding + config.stride,
                           shape_l[2] + 2 * padding + config.stride, shape_l[3]))

        # 把数据填充到空白的大图中
        input_[:, padding:-config.stride - padding,
               padding:-config.stride - padding, :] = input
        label_[:, padding:-config.stride - padding,
               padding:-config.stride - padding, :] = label

        print("new image size(after padding):")
        print(input_.shape)
        print("new label size(after padding):")
        print(label_.shape)
        count = 0
        for i in range(len(input_)):
            # 每一个时次, 也就是每一张大图
            if len(input_[i].shape) == 3:
                h, w, _ = input_[i].shape
            else:
                h, w = input_[i].shape

            # 切小图
            for x in range(0, h - config.patch_size + 1, config.stride):
                for y in range(0, w - config.patch_size + 1, config.stride):
                    sub_input = input_[i, x:x + config.patch_size,
                                       y:y + config.patch_size]  # [105 x 105 x 43]

                    # 这一步直接完成了将B切出的每个105*105的小图中间93*93部分抠出来
                    sub_label = label_[i, x + padding:x + padding + patch_size_l,
                                       y + padding:y + padding + patch_size_l]  # [93 x 93 x 41]

                    sub_input_sequence.append(sub_input)
                    sub_label_sequence.append(sub_label)

        print('trainset.shape: ', np.array(sub_input_sequence).shape)

    else:
        # test part.
        input = preprocess_reconstruct(config)  # (-1, w, j, 43)
        shape_i = input.shape
        #shape_l = label.shape

        patch_size = config.patch_size
        patch_size_l = config.patch_size_l
        padding = (patch_size - patch_size_l) // 2

        # padding 681+6+(6+93), 1081+6+(6+93)
        input_ = np.zeros((shape_i[0], shape_i[1] + 2 * padding + config.stride,
                           shape_i[2] + 2 * padding + config.stride, shape_i[3]))
        #label_ = np.zeros((shape_l[0], shape_l[1]+padding+config.stride, shape_l[2]+padding+config.stride, shape_l[3]))

        # 填充
        input_[:, padding:-config.stride - padding,
               padding:-config.stride - padding, :] = input
        #label_[:,padding:-config.stride,padding:-config.stride,:] = label

        for i in range(len(input_)):

            if len(input_[i].shape) == 3:
                h, w, _ = input_[i].shape
            else:
                h, w = input_[i].shape

            # Numbers of sub-images in height and width of image are needed to compute merge operation.
            nx = ny = 0
            # 切小图
            for x in range(0, h - config.patch_size + 1, config.stride):
                nx += 1
                ny = 0
                for y in range(0, w - config.patch_size + 1, config.stride):
                    ny += 1
                    sub_input = input_[i, x:x + patch_size,
                                       y:y + patch_size]  # [33 x 33 x 43]

                    sub_input_sequence.append(sub_input)

            # 如果一个大图被切分成12x18块， 那么nxy = [[12,18], [12,18], ...], len(nxy)=shape_i[0]
            nxy.append([nx, ny])

    # Make list to numpy array. With this transform
    arrdata = np.asarray(sub_input_sequence)  # [?, patch_size, patch_size, 43]
    if config.is_train:
        # [?, patch_size_l, patch_size_l, 41]
        arrlabel = np.asarray(sub_label_sequence)

    # 根据is_train 输出
    if not config.is_train:
        return nxy, arrdata
    else:
        print("now prepare the train data and label data.......")
        print("train data shape is:")
        print(arrdata.shape)
        print("label data shape is:")
        print(arrlabel.shape)
        return arrdata, arrlabel


def imsave(image, path):
    # return scipy.misc.imsave(path, image)
    return plt.imsave(path, image)


def merge(images, size, config):
    """将经过模型的小图拼接成大图

    例如, images [n, 93, 93, 41], size [8, 12]
    得到的就是[8*93, 12*93, 41]的大图
    最后返回的时候 再裁剪一下

    """
    h, w, channel = images.shape[1], images.shape[2], images.shape[3]
    img = np.zeros((h * size[0], w * size[1], channel))
    for idx, image in enumerate(images):
        i = idx % size[1]
        j = idx // size[1]
        img[j * h:j * h + h, i * w:i * w + w] = image

    return img[:config.label_size_w, :config.label_size_j, :]


def parse_config_test(config_path, section, option):
    """从配置文件中获取对应的infos
    param: config_path: .config文件的路径
           section: layer_index, ex. 1_1 1_2
           option: 具体的选项, label, feature..

    """
    cp = configparser.ConfigParser()

    if not os.path.exists(config_path):
        print("NO configuration:{0} !".format(config_path))
        exit()
    cp.read(config_path)
    return cp.get(section, option)


def checkTwoLabelZoom(config):
    """将得到的label：
                    1. 下采样+插值
                    2. 直接下采样
                    保存两份nc文件
    """
    input_, label_ = get_data(config.label)

    scale0w = config.input_size_w / label_.shape[-3]
    scale0j = config.input_size_j / label_.shape[-2]
    img_ = scipy.ndimage.zoom(label_, (1, scale0w, scale0j, 1))
    # resize the input data and feature data from input_size to image_size
    scalew = config.image_size_w / config.input_size_w
    scalej = config.image_size_j / config.input_size_j
    # 插值, 得到粗糙的数据(2.5KM) 也就是doc里的 'A'
    img_ = scipy.ndimage.interpolation.zoom(img_, (1, scalew, scalej, 1))
    #img_ = np.maximum(img_,0)
    img_ = np.where(img_ < 0, 0, img_)
    img_ = np.where(img_ > 9999, 0, img_)
    shap = img_.shape
    img_ = img_.reshape(shap[0], shap[3], shap[1], shap[2])

    # 对于较为精确的数据'B', 直接下采样的2.5KM
    scale2w = config.label_size_w / label_.shape[-3]
    scale2j = config.label_size_j / label_.shape[-2]
    label_ = scipy.ndimage.zoom(label_, (1, scale2w, scale2j, 1))
    label_ = np.where(label_ < 0, 0, label_)
    shap = label_.shape
    label_ = label_.reshape(shap[0], shap[3], shap[1], shap[2])

    # 将img_, label_都保存成nc文件
    outncA = []
    outncB = []
    outncA.append([0])  # time
    outncB.append([0])  # time
    isobaric = [2500 + i * 2500 for i in range(1, 42)]
    lat = [36 - 0.05 * i for i in range(0, 281)]
    lon = [108 + 0.05 * i for i in range(0, 321)]

    outncA.append(isobaric)
    outncA.append(lat)
    outncA.append(lon)
    outncA.append(img_)
    writenc3D('./cucao.nc', outncA, config)

    outncB.append(isobaric)
    outncB.append(lat)
    outncB.append(lon)
    outncB.append(label_)
    writenc3D('./jingque.nc', outncB, config)


if __name__ == '__main__':
    # 测试二维模型得到的训练数据的shape
    flags = tf.app.flags
    flags.DEFINE_string('input', 'label\\3\\humidity', 'input directory')
    flags.DEFINE_string('label', 'label\\3\\humidity', 'label directory')
    flags.DEFINE_string('feature', 'dem\\3', 'feature directory')
    flags.DEFINE_string('category', 'humidity', '气象要素')

    # 对应的是EC模式，3_1
    flags.DEFINE_integer('input_size_w', 113, 'The input size w')
    flags.DEFINE_integer('input_size_j', 129, 'The input size j')
    flags.DEFINE_integer('image_size_w', 281, 'The image size w')
    flags.DEFINE_integer('image_size_j', 321, 'The image size j')
    flags.DEFINE_integer('label_size_w', 281, 'The label size w')
    flags.DEFINE_integer('label_size_j', 321, 'The label size j')

    flags.DEFINE_integer("patch_size", 40, "the size of sub images")
    flags.DEFINE_integer("patch_size_l", 28,
                         "the size of sub label images")
    flags.DEFINE_integer(
        "stride", 20, "The size of stride to apply input image [14]")

    flags.DEFINE_boolean(
        "is_train", True, "True for training, False for testing [True]")

    FLAGS = flags.FLAGS

    # input, label = preprocess(FLAGS)
    # print(input.shape)

    # print(label.shape)

    # train_data, train_label = input_setup(FLAGS)
    # print(train_data.shape)
    # print(train_label.shape)

    # nxy, train_data = input_setup(FLAGS)
    # print(train_data.shape)
    # print(nxy)

    # images = np.random.rand(224, 28, 28, 41)
    # size = [14, 16]
    # flags.DEFINE_integer('label_size_w', 281, 'The label size w')
    # flags.DEFINE_integer('label_size_j', 321, 'The label size j')
    # FLAGS = flags.FLAGS

    # res = merge(images=images, size=size, config=FLAGS)
    # print(res.shape)

    # checkpoint_dir = parse_config_test(
    #     './configuration/train_config/EC.conf', '3_1', "checkpoint_" + "humidity")
    # print(checkpoint_dir)

    checkTwoLabelZoom(FLAGS)
