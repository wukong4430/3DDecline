import sys
sys.path.append("..")
from model import SRCNN
from utils import input_setup
from utils import parse_config_test
import numpy as np
import tensorflow as tf

import pprint
import os

# In the train.py we receive 2 parameters as follows:
# mode_str:The name of mode. Like EC/NMC/RJTD and so on.
# layer_index:The combination of area number and layer number,
#             For example:1_1 means the first area and the first layer.
# category: humidity/temperature/windu/windv

try:
    mode_str = sys.argv[1].upper()
    layer_index = sys.argv[2]
    category = sys.argv[3].upper()
except Exception:
    print("The parameter is wrong! check it please")
    exit()

# Firstly, we get the configuration file path via the mode_str.
config_file_name = mode_str + ".conf"

# The we can get the configuration file path.
config_file_path = os.path.join(os.getcwd(), r"configuration/train_config")
config_file_path = os.path.join(config_file_path, config_file_name)

# 到这里把配置文件的绝对路径拼出来了
print("The train configuration file is:{0}".format(config_file_path))

# Then we can read parameters via the layer_index in the config file.

epoch = 3000
try:
    label = parse_config_test(config_file_path, layer_index, category)  # 路径名字
    feature = parse_config_test(
        config_file_path, layer_index, "feature")  # 路径名字

    input_size_j = parse_config_test(
        config_file_path, layer_index, "input_size_j")
    input_size_w = parse_config_test(
        config_file_path, layer_index, "input_size_w")
    image_size_j = parse_config_test(
        config_file_path, layer_index, "image_size_j")
    image_size_w = parse_config_test(
        config_file_path, layer_index, "image_size_w")
    label_size_j = parse_config_test(
        config_file_path, layer_index, "label_size_j")
    label_size_w = parse_config_test(
        config_file_path, layer_index, "label_size_w")

    # patch_size, 大图切小图时, 小图的尺寸
    # NMC 1_1 是 105.
    patch_size = parse_config_test(config_file_path, layer_index, "patch_size")

    # patch_size_l, 经过卷积层之后的大小.
    # NMC 1_1 是 93.
    patch_size_l = parse_config_test(
        config_file_path, layer_index, "patch_size_l")

    batch_size = parse_config_test(
        config_file_path, "common_option", "batch_size")
    learning_rate = parse_config_test(
        config_file_path, "common_option", "learning_rate")
    c_dim = parse_config_test(config_file_path, "common_option", "c_dim")

    # 小图移动时的步长大小
    # NMC 1_1 是 60.
    stride = parse_config_test(config_file_path, layer_index, "stride")
    checkpoint_dir = parse_config_test(
        config_file_path, layer_index, "checkpoint_" + category)

    # change the type:str to int or float
    input_size_j = int(input_size_j)
    input_size_w = int(input_size_w)
    image_size_j = int(image_size_j)
    image_size_w = int(image_size_w)
    label_size_j = int(label_size_j)
    label_size_w = int(label_size_w)
    patch_size = int(patch_size)
    patch_size_l = int(patch_size_l)
    batch_size = int(batch_size)
    learning_rate = float(learning_rate)
    c_dim = int(c_dim)
    stride = int(stride)
except Exception as e:
    print("There is a error:", e)
    exit()


# define the tf.app.flags and send these parameters to model.py
flags = tf.app.flags
flags.DEFINE_integer("epoch", epoch, "Number of epoch [10]")
flags.DEFINE_string("label", label, "label directory")
flags.DEFINE_string("feature", feature, "feature directory")
flags.DEFINE_string("category", category, "category discrpition")
flags.DEFINE_string("mode", mode_str, "mode discrpition")
flags.DEFINE_string("layer_index", layer_index, "layer_index discrpition")

flags.DEFINE_integer("input_size_j", input_size_j,
                     "The size of input image to use[141]")
flags.DEFINE_integer("input_size_w", input_size_w,
                     "The size of input image to use[141]")

flags.DEFINE_integer("image_size_j", image_size_j,
                     "The size of image to use [281]")
flags.DEFINE_integer("image_size_w", image_size_w,
                     "The size of image to use [281]")
flags.DEFINE_integer("label_size_j", label_size_j,
                     "The size of label to produce [281]")
flags.DEFINE_integer("label_size_w", label_size_w,
                     "The size of label to produce [281]")

flags.DEFINE_integer("patch_size", patch_size, "the size of sub images")
flags.DEFINE_integer("patch_size_l", patch_size_l,
                     "the size of sub label images")

flags.DEFINE_integer("batch_size", batch_size, "The size of batch images [64]")
flags.DEFINE_float("learning_rate", learning_rate,
                   "The learning rate of gradient descent algorithm [1e-4]")
flags.DEFINE_integer("c_dim", c_dim, "Dimension of image color. [1]")

flags.DEFINE_integer(
    "stride", stride, "The size of stride to apply input image [14]")
flags.DEFINE_string("checkpoint_dir", checkpoint_dir,
                    "Name of checkpoint directory [checkpoint]")

flags.DEFINE_boolean(
    "is_train", True, "True for training, False for testing [True]")


def main(_):
    FLAGS = flags.FLAGS

    pp = pprint.PrettyPrinter()
    FLAGS._parse_flags()
    pp.pprint(flags.FLAGS.__flags)

    if not os.path.exists(FLAGS.checkpoint_dir):
        os.makedirs(FLAGS.checkpoint_dir)

    # gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.4)
    # with tf.Session(config=tf.ConfigProto(gpu_options=gpu_options)) as sess:

    with tf.Session() as sess:
        srcnn = SRCNN(sess,
                      input_size_w=FLAGS.input_size_w,
                      image_size_w=FLAGS.image_size_w,
                      label_size_w=FLAGS.label_size_w,
                      input_size_j=FLAGS.input_size_j,
                      image_size_j=FLAGS.image_size_j,
                      label_size_j=FLAGS.label_size_j,
                      batch_size=FLAGS.batch_size,
                      patch_size=FLAGS.patch_size,
                      patch_size_l=FLAGS.patch_size_l,
                      c_dim=FLAGS.c_dim,
                      checkpoint_dir=FLAGS.checkpoint_dir,
                      is_train=FLAGS.is_train)

        srcnn.train(FLAGS)


if __name__ == '__main__':
    tf.app.run()
