import netCDF4 as nc
import numpy as np
import zlib
import os

# def writenc(name, data):
#   """
#   write all data into xxx.nc file.
#   将得到的降尺度数据写入.nc文件中, 此函数一般不需要改动,直接使用即可
#   param: name: 文件的路径
#          data: 具体的数据文件, 保存在list中
#   """
#     dataset = nc.Dataset(name, 'w', format='NETCDF4', zlib=True)  # 'NETCDF4_CLASSIC')
#     keys = ['time', 'lat', 'lon', 'precipitation']
#     # 创建'time' Dimension.
#     dataset.createDimension(keys[0], len(data[0]))
#     dataset.createVariable(keys[0], np.float64, (keys[0]), zlib=True)
#     dataset.variables[keys[0]][:] = data[0]

#     # 创建 'lat', 'lon',  Dimension.
#     for i in range(1, len(keys) - 1):
#         dataset.createDimension(keys[i], len(data[i]))
#         dataset.createVariable(keys[i], np.float32, (keys[i]), zlib=True)
#         dataset.variables[keys[i]][:] = data[i]

#     # 创建 'precipitation'
#     dataset.createVariable(keys[-1], np.float32, tuple(keys[:-1]), zlib=True)

#     d = np.array(data[-1])  # = np.where(data[-1][:]<0.1,0,data[-1][:])
#     # print(d.shape)
#     d = np.where(d < 0.01, 0, d)
#     # data[-1] = data[-1].round(2)
#     dataset.variables[keys[-1]][:] = d  # np.maximum(data[-1],0)
#     tt = name.split('_')[-2]
#     yy, mm, dd, hh = tt[0:4], tt[4:6], tt[6:8], tt[8:10]
#     time = dataset.variables['time']
#     time.units = 'minutes since %s-%s-%s %s:00:00' % (yy, mm, dd, hh)
#     time.calendar = 'gregorian'
#     dataset.variables['lat'].units = 'degrees_north'
#     dataset.variables['lon'].units = 'degrees_east'
#     dataset.variables['precipitation'].units = 'mm'
#     dataset.close()


def writenc3D(name, data, config):
    """
    write all data into xxx.nc file.
    将得到的降尺度数据写入.nc文件中, 此函数一般不需要改动,直接使用即可
    param: name: 文件的路径
           data: 具体的数据文件, 保存在list中
    """

    dataset = nc.Dataset(name, 'w', format='NETCDF4', zlib=True)
    flag = None
    if config.category == "humidity":
        keys = ['time', 'isobaric', 'lat', 'lon', 'Relative_humidity_isobaric']
        flag = 'H'
    elif config.category == "temperature":
        keys = ['time', 'isobaric', 'lat', 'lon', 'Temperature_isobaric']
        flag = 'T'
    elif config.category == "windu":
        keys = ['time', 'isobaric', 'lat', 'lon',
                'u-component_of_wind_isobaric']
        flag = 'U'
    elif config.category == "windv":
        keys = ['time', 'isobaric', 'lat', 'lon',
                'v-component_of_wind_isobaric']
        flag = 'V'
    else:
        print("!!!!!!!!!!!!!!!!!!!!!Wrong category accepted!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        os.exit()

    for i in range(0, len(keys) - 1):
        dataset.createDimension(keys[i], len(data[i]))
        if i == 0:
            dataset.createVariable(keys[i], np.float64, (keys[i],), zlib=True)
        else:
            dataset.createVariable(keys[i], np.float32, (keys[i],), zlib=True)
        dataset.variables[keys[i]][:] = data[i]

    dataset.createVariable(keys[-1], np.float32, tuple(keys[:-1]), zlib=True)
    d = np.array(data[-1])
    # d = np.where(d < 0.001, 0, d)

    dataset.variables[keys[-1]][:] = d

    time = dataset.variables['time']
    # time.units = 'hours since 2016-06-01 00:00:00'
    tt = name.split('_')[-2]
    yy, mm, dd, hh = tt[0:4], tt[4:6], tt[6:8], tt[8:10]
    time = dataset.variables['time']
    time.units = 'minutes since %s-%s-%s %s:00:00' % (yy, mm, dd, hh)
    time.calendar = 'gregorian'

    isobaric = dataset.variables['isobaric']
    isobaric.units = 'hPa'
    isobaric.positive = 'down'

    dataset.variables['lat'].units = 'degrees_north'
    dataset.variables['lon'].units = 'degrees_east'

    if flag == 'H':
        dataset.variables['Relative_humidity_isobaric'].units = '%'
    elif flag == 'T':
        dataset.variables['Temperature_isobaric'].units = 'K'
    elif flag == 'U':
        dataset.variables['u-component_of_wind_isobaric'].units = 'm/s'
    elif flag == 'V':
        dataset.variables['v-component_of_wind_isobaric'].units = 'm/s'
    else:
        pass
    dataset.close()


def main():
    testData = np.random.rand(1, 421, 481, 41)
    print(testData.shape)
    outnc = []
    outnc.append([30])  # time
    isobaric = [2500 + i * 2500 for i in range(1, 42)]
    lat = [36 - 10 / 3 * i for i in range(1, 422)]
    lon = [108 + 10 / 3 * i for i in range(1, 482)]
    outnc.append(isobaric)
    outnc.append(lat)
    outnc.append(lon)
    outnc.append(testData)

    writenc3D('./resultTest.nc', outnc)


if __name__ == '__main__':
    main()
