import sys
sys.path.append("..")
import os
from utils import parse_config_test
import shutil
from multiprocessing import Process
from test import *
from read_nc import get_name
# GPU10 is not exists! So it will use CPU instead!
os.environ["CUDA_VISIBLE_DEVICES"] = "10"

# we accept two parameters from the shell or user input.
# The first parameter is mode str. like NMC,EC,RJTD and so on.
# The second parameter is time str.The format like this:yyyymmddHH.
# An example:2018070400 or 2018070412. If we don't accept the two parameters.
# we will exit this program.
try:
    mode_str = sys.argv[1].upper()
    time_str = sys.argv[2]
    category = sys.argv[3].upper()
except Exception as e:
    print("The parameter is wrong! check it please")
    exit()

print(
    "-------------------Start to deal with the mode:{0}------------------------".format(mode_str))

# Then We set a loop to processe the 8 areas
for i in range(3, 4):
    i = str(i)
    print("==========================================")
    print("Now we start to process the area {0}".format(i))

    # we can get the configuration file name according to the mode str!
    config_name = mode_str + ".conf"

    # The we can read the configuration file according it's path.
    config_file_path = os.path.join(os.getcwd(), "configuration/test_config")
    config_file_path = os.path.join(config_file_path, config_name)
    print("The config file path is:{0}".format(config_file_path))

    # we get the area name according the "i"
    area_name = parse_config_test(config_file_path, i, "area_name")

    # We get the input low resolution(lr) file name and the input dir.
    # Furthermore we can get the low resolution file path.
    input_lr_file = mode_str + "_" + time_str + \
        "_" + area_name + "_" + category + ".nc"
    input_lr_dir = parse_config_test(
        config_file_path, "input", "input_file_dir_path")
    input_lr_file_path = os.path.join(input_lr_dir, input_lr_file)

    # We can check if the input_lr_file_path exists. We set a flag.
    flag = False
    if os.path.exists(input_lr_file_path):
        print("The low resolution file:{0} is existing!!".format(
            input_lr_file_path))
        flag = True

       # If the input_lr_file_path is not existing, We continue to process the next area.
    if not flag:
        print("The low resolution file:{0} is not existing!".format(
            input_lr_file_path))
        continue

    # Here we can gurantee the lr file exists. We can start our deepsd.
    # Firstly we copy the lr file to the tmp_testset dir.
    # An example:/home/pwq/models_simplify/tmp_testset/EC_testset/2018062400/1/humidity
    test_set_name = mode_str + "_" + "testset"  # EC_testset
    test_set_path = os.path.join(os.getcwd(), "tmp_testset")
    test_set_path = os.path.join(test_set_path, test_set_name)

    test_set_date_path = os.path.join(test_set_path, time_str)
    test_set_path = os.path.join(test_set_date_path, i)
    test_set_path = os.path.join(test_set_date_path, category)
    print("The test set path of this area is:{0}".format(test_set_path))

    # Secondly we should ensure the tmp output dir. There are n tmp results if it has n layers.
    # So we should know how many layers in this mode.
    locdir = []
    locdir.append(test_set_path)
    layer_number = parse_config_test(config_file_path, "layer", "number")
    layer_number = int(layer_number)  # 取出某个模式下的网络层数， EC就是两层。

    print("The pwd is:{0}".format(os.getcwd()))
    for j in range(layer_number):
        output = os.path.join(os.getcwd(), "tmp_output")
        output = os.path.join(output, "{0}_output".format(mode_str))
        output_date_dir = os.path.join(output, time_str)
        output = os.path.join(output_date_dir, "{0}".format(i))
        output_category = os.path.join(output, "{0}".format(category))
        output = os.path.join(output_category, "output{0}".format(str(j + 1)))
        # Here an example of EC's output like:
        #/home/pwq/models_simplify/tmp_output/EC_output/2018062412/1/humidity/output1 # 保存的是EC的第一层结果
        #/home/pwq/models_simplify/tmp_output/EC_output/2018062412/1/humidity/output2 # 保存的是EC的第二层结果
        # Because there are two layers in the EC's deepsd model.

        # An another example of RJTD's output like:
        #/home/pwq/models_simplify/tmp_output/RJTD_output/2018062412/1/humidity/output1
        #/home/pwq/models_simplify/tmp_output/RJTD_output/2018062412/1/humidity/output2
        #/home/pwq/models_simplify/tmp_output/RJTD_output/2018062412/1/humidity/output3
        # Because there are 3 layers in the RJTD's deepsd model.
        locdir.append(output)

    """
    现在locdir是：[
    './tmp_testset/EC_testset/2018062400/1/humidity',
    './tmp_output/EC_output/2018062412/1/humidity/output1',
    './tmp_output/EC_output/2018062412/1/humidity/output2'
    ]
    """

    # And we re-create the test_set_path to accept the lr file.
    if not os.path.exists(test_set_path):
        os.makedirs(test_set_path)
    shutil.copy(input_lr_file_path, test_set_path)

    # If you want to delete the original lr file, Open the next line's annotation.
    # os.remove(input_lr_file_path)

    # we get the final file name via the file in the test_set_path
    try:
        final_file_name = get_name(test_set_path)
    except Exception as e:
        print("During get the final file name, There is a error:", e)

    # We get the output dir according to the config to save the final hr file.
    output_hr_dir = parse_config_test(
        config_file_path, "output", "output_file_dir_path")
    yy = time_str[:4]
    yymmdd = time_str[:8]

    output_hr_dir_path = os.path.join(output_hr_dir, yy)
    # ./outputdir/2018/20180120
    output_hr_dir_path = os.path.join(output_hr_dir_path, yymmdd)

    # we get the final hr file path via the output_hr_dir_path and the names
    final_hr_abspath = os.path.join(
        output_hr_dir_path, "".join(final_file_name))
    print("The final hr file aspath is:{0}".format(final_hr_abspath))

    # If the final hr file is exists. we continue the next loop.
    if os.path.exists(final_hr_abspath):
        print("The final sp1 file:{0} is existing!".format(final_file_name))
        shutil.rmtree(test_set_date_path)
        continue
    # If the final output dir is not existing, We create it.
    if not os.path.exists(output_hr_dir_path):
        os.makedirs(output_hr_dir_path)

    print(locdir)

    if flag:
        print("the lr file to be processed is:{0}".format(input_lr_file_path))
        print("the output hr file of area is stored in:{0}".format(
            output_hr_dir_path))

        layer_number = int(layer_number)
        # We know the layer number, Which means we should do how many times spatial downscale.

        print("The mode {0} has {1} layers".format(
            mode_str, str(layer_number)))

        # Since that we know how many layers in current mode, we set a loop to deal with it.
        # In this loop, we can deduce the layer_index by "i" and "k", "i" means the ith area.
        # The k means kth layer of current mode.

        # And we call the fuction "run_test" from the test.py file. Sending parameters as follow:
        # config_file_path :The absolutely path of the configuration file we need to read.
        # test_set_path:we need get some information from the lr file.
        # layer_index:we can precisely locate which area and which layer we need to process.
        # locdir[k]:The input dir of the deepsd model.
        # locdir[k+1]:The temporary output dir of deepsd model.

        # Here is a brief explanation of locdir[k] and locdir[k+1]:in first layer, the input dir
        # is test set(locdir[0]), the output dir is output1(locdir[1]). In second layer, the input
        # dir is output1(locdir[1]), the output dir is output2(locdir[2]).......

        for k in range(layer_number):
            layer_index = i + "_" + str(k + 1)
            print("The current layer_index is:{0}".format(layer_index))
            try:
                p1 = Process(target=run_test, args=(config_file_path,
                                                    test_set_path, layer_index, locdir[k], locdir[k + 1], category))
                p1.start()
            except Exception as e:
                print("There is a error:", e)
            p1.join()

        print("The area{0}'s sp1 finished!".format(i))
        print("Now we sent the tmp output file to the final output dir.......")

        # The final output file now is in the temp output dir.For example, In the mode EC, the
        # final output file is in the output2 because there are 2 layers in the EC model.
        # Which means the final output file is in the locdir[-1].

        f = os.listdir(locdir[-1])
        print("The file in temp_output is:{0}".format(f))
        if len(f) > 1:
            print("The temp output dir:{0} has more than one file! please check it!".format(
                locdir[-1]))
            exit()
        elif len(f) == 0:
            print(
                "The temp output dir:{0} has no file! please check it!!".format(locdir[-1]))
            exit()
        else:
            files = f[0]

        send_file_path = os.path.join(locdir[-1], files)
        print("The send file path is:{0}".format(send_file_path))
        shutil.copy(send_file_path, output_hr_dir_path)
        print("Send the file to final dir complete!")

        print("start to delete the tmp dir in the project......")
        print("deleting the test set dir:{0}".format(test_set_date_path))
        print("deleting the tmp output dir:{0}".format(output_date_dir))
        try:
            shutil.rmtree(test_set_date_path)
            shutil.rmtree(output_date_dir)
        except Exception as e:
            print("During deleting the tmp dir, There is a error:", e)
    else:
        print("No new update files like {0}".format(time_str))
        exit()
