import sys
sys.path.append("..")
from model import SRCNN
from utils import input_setup
from utils import parse_config_test
import numpy as np
import tensorflow as tf

import pprint
import os
os.environ["CUDA_VISIBLE_DEVICES"] = "4"


def run_test(config_file_path, test_set_path, layer_index, input_dir, output_dir, category):
  '''
  @author:pwq
  @date:20180705
  @para:
        1.config_file_path:the absolutely path of configuration path.
        2.test_set_path:The path of test set dir

        3.layer_index:we can use it to precisely locate which area and which layer we need to process.
                       "i_k" ,"i" means the ith area.The k means kth layer of current mode.
        4.input_dir:The input dir that need to feed into the deepsd.
        5.output_dir:The dir that used for saving the deepsd result.

  @function:This method is used for testing the deepsd model. 
            We can use this func to achieve the mapping of low resolution to high 
            resolution.In this method, we firstly read some parameters from the 
            configuration file according to the config_file_path.
            But how to locate the parameters in the config file ? It depends on the layer_index.
  '''
  try:
    input_ = input_dir  # 如果input_dir对应1_1, output_dir对应1_2
    output_ = output_dir
    label = parse_config_test(config_file_path, layer_index, "label")
    feature = parse_config_test(config_file_path, layer_index, "feature")
    time_dimen = test_set_path

    checkpoint_dir = parse_config_test(
        config_file_path, layer_index, "checkpoint_" + category)
    input_size_j = parse_config_test(
        config_file_path, layer_index, "input_size_j")
    input_size_w = parse_config_test(
        config_file_path, layer_index, "input_size_w")
    image_size_j = parse_config_test(
        config_file_path, layer_index, "image_size_j")
    image_size_w = parse_config_test(
        config_file_path, layer_index, "image_size_w")
    label_size_j = parse_config_test(
        config_file_path, layer_index, "label_size_j")
    label_size_w = parse_config_test(
        config_file_path, layer_index, "label_size_w")

    patch_size = parse_config_test(
        config_file_path, layer_index, "patch_size")
    patch_size_l = parse_config_test(
        config_file_path, layer_index, "patch_size_l")
    batch_size = parse_config_test(
        config_file_path, layer_index, "batch_size")
    learning_rate = parse_config_test(
        config_file_path, layer_index, "learning_rate")
    c_dim = parse_config_test(config_file_path, layer_index, "c_dim")
    stride = parse_config_test(config_file_path, layer_index, "stride")

    # change the type:str to int or float
    input_size_j = int(input_size_j)
    input_size_w = int(input_size_w)
    image_size_j = int(image_size_j)
    image_size_w = int(image_size_w)
    label_size_j = int(label_size_j)
    label_size_w = int(label_size_w)
    patch_size = int(patch_size)
    patch_size_l = int(patch_size_l)
    batch_size = int(batch_size)
    learning_rate = float(learning_rate)
    c_dim = int(c_dim)
    stride = int(stride)
  except Exception as e:
    print("There is a error:", e)

  # define the tf.app.flags
  # 第一个是参数名称，第二个参数是默认值，第三个是参数描述
  flags = tf.app.flags
  flags.DEFINE_string("input", input_, "input directory")
  flags.DEFINE_string("label", label, "label directory")
  flags.DEFINE_string("feature", feature, "feature directory")
  # time_dimen means the output result's time dimen come from test_set folder
  flags.DEFINE_string("time_dimen", time_dimen, "feature directory")

  flags.DEFINE_integer("input_size_j", input_size_j,
                       "The size of input image to use[141]")
  flags.DEFINE_integer("input_size_w", input_size_w,
                       "The size of input image to use[141]")
  flags.DEFINE_integer("image_size_j", image_size_j,
                       "The size of image to use [281]")
  flags.DEFINE_integer("image_size_w", image_size_w,
                       "The size of image to use [281]")
  flags.DEFINE_integer("label_size_j", label_size_j,
                       "The size of label to produce [281]")
  flags.DEFINE_integer("label_size_w", label_size_w,
                       "The size of label to produce [281]")
  flags.DEFINE_integer("patch_size", patch_size, "the size of sub images")
  flags.DEFINE_integer("patch_size_l", patch_size_l,
                       "the size of sub label images")

  flags.DEFINE_integer("batch_size", batch_size,
                       "The size of batch images [64]")
  flags.DEFINE_float("learning_rate", learning_rate,
                     "The learning rate of gradient descent algorithm [1e-4]")
  flags.DEFINE_integer("c_dim", c_dim, "Dimension of image color. [1]")
  flags.DEFINE_integer(
      "stride", stride, "The size of stride to apply input image [14]")
  flags.DEFINE_string("checkpoint_dir", checkpoint_dir,
                      "Name of checkpoint directory [checkpoint]")
  flags.DEFINE_string("sample_dir", output_,
                      "Name of sample directory [output30]")

  # is_train==False, means use the model.
  flags.DEFINE_boolean(
      "is_train", False, "True for training, False for testing [True]")

  # 各种参数已经配置完毕.
  # 接下来就是调用模型

  FLAGS = flags.FLAGS

  pp = pprint.PrettyPrinter()
  FLAGS._parse_flags()
  pp.pprint(flags.FLAGS.__flags)

  if not os.path.exists(FLAGS.sample_dir):
    os.makedirs(FLAGS.sample_dir)
  with tf.Session() as sess:
    srcnn = SRCNN(sess,
                  input_size_w=FLAGS.input_size_w,
                  image_size_w=FLAGS.image_size_w,
                  label_size_w=FLAGS.label_size_w,
                  input_size_j=FLAGS.input_size_j,
                  image_size_j=FLAGS.image_size_j,
                  label_size_j=FLAGS.label_size_j,

                  batch_size=FLAGS.batch_size,
                  patch_size=FLAGS.patch_size,
                  patch_size_l=FLAGS.patch_size_l,
                  c_dim=FLAGS.c_dim,
                  checkpoint_dir=FLAGS.checkpoint_dir,
                  is_train=FLAGS.is_train)

    srcnn.train(FLAGS)
  pass


'''
def main(_):
  pp.pprint(flags.FLAGS.__flags)

  if not os.path.exists(FLAGS.checkpoint_dir):
    os.makedirs(FLAGS.checkpoint_dir)
  if not os.path.exists(FLAGS.sample_dir):
    os.makedirs(FLAGS.sample_dir)

  with tf.Session() as sess:
    srcnn = SRCNN(sess, 
                  input_size_w=FLAGS.input_size_w,
                  image_size_w=FLAGS.image_size_w,
                  label_size_w=FLAGS.label_size_w,
                  input_size_j=FLAGS.input_size_j,
                  image_size_j=FLAGS.image_size_j,
                  label_size_j=FLAGS.label_size_j,

                  batch_size=FLAGS.batch_size,
                  patch_size=FLAGS.patch_size,
                  patch_size_l=FLAGS.patch_size_l,
                  c_dim=FLAGS.c_dim, 
                  checkpoint_dir=FLAGS.checkpoint_dir,
                  sample_dir=FLAGS.sample_dir)

    srcnn.train(FLAGS)

if __name__ == '__main__':
  tf.app.run()
'''
