import netCDF4 as nc
import os
import glob
import scipy.ndimage
import numpy as np
import sys


def prepare_data(dataset):
    '''获取当前文件夹里，所有nc文件的路径列表
    param: dataset: 文件夹路径
    return: List['.nc']
    '''
    if not os.path.exists(dataset):
        print(" No such file or directory:{0}".format(dataset))
        exit()
    # 获取ｎｃ文件路径列表
    filenames = os.listdir(dataset)
    # os.getcwd()：获取当前工作目录，也就是在哪个目录下运行这个程序。
    data_dir = os.path.join(os.getcwd(), dataset)
    # glob函数用来查找符合指定规则的文件路径名
    data = glob.glob(os.path.join(data_dir, "*.nc"))

    return data


def get_time_dimen(config):
    """获取时间维度

    return: List[分钟]
    example: [ 180.  360.  540.  720.  900. 1080. 1260. 1440. 1620. 1800. 1980. 2160.
                2340. 2520. 2700. 2880. 3060.]
    """
    paths = prepare_data(config.time_dimen)
    if len(paths) == 0:
        print("There is no file in the:{0}".format(config.time_dimen))
        exit()

    # 该文件夹下所有的.nc文件的时间都是一样的, 所以只需要选一个文件就可以了.
    # path是一个.nc文件的绝对路径
    path = paths[0]

    dataset = nc.Dataset(path)

    time = dataset.variables["time"][:]

    # *60.0 means change hours into minutes
    time = time * 60.0
    dataset.close()
    return time


def get_isobaric(config):
    """获取等压面

    return: List[hPa]
    example: [  5000.   7500.  10000.  12500.  15000.  17500.  20000.  22500.  25000.
  27500.  30000.  32500.  35000.  37500.  40000.  42500.  45000.  47500.
  50000.  52500.  55000.  57500.  60000.  62500.  65000.  67500.  70000.
  72500.  75000.  77500.  80000.  82500.  85000.  87500.  90000.  92500.
  95000.  97500. 100000. 102500. 105000.]
    """
    paths = prepare_data(config.time_dimen)
    if len(paths) == 0:
        print("There is no file in the:{0}".format(config.time_dimen))
        exit()

    # 该文件夹下所有的.nc文件的时间都是一样的, 所以只需要选一个文件就可以了.
    # path是一个.nc文件的绝对路径
    path = paths[0]

    dataset = nc.Dataset(path)

    isobaric = dataset.variables['isobaric'][:]

    dataset.close()

    return isobaric


def get_lat_lon_dimen(config):
    """下采样经度和维度, 只在test 使用

    param: config.label永远是1KM的数据
    return: 下采样后(长度变短)的数据.

    """
    paths = prepare_data(config.label)
    if len(paths) == 0:
        print("There is no file in the:{0}".format(config.label))
        exit()
    path = paths[0]
    dataset = nc.Dataset(path)
    data = dataset.variables["lat"][:]

    # len(data)一定大于等于config.label_size_w, 比如NMC的[1]NEC区域, len(data)==1701, 第一层label_size_w==681, 第二层的label_size_w==1701
    lat = scipy.ndimage.zoom(data, (config.label_size_w / len(data)))
    data = dataset.variables["lon"][:]

    # 维度与上述经度相似
    lon = scipy.ndimage.zoom(data, (config.label_size_j / len(data)))
    dataset.close()
    # return d
    return lat, lon


def get_name(name):
    """将类似
    'NMC_2018102000_CCN.nc'
            文件名字转化成 
        "MSP2_PMSC_AIWSRPF_NMCSP1_L88_CCN_201810200000_00000-05100.nc"
    """
    paths = prepare_data(name)
    paths = sorted(paths)
    if len(paths) == 0:
        print("There is no file in the:{0}".format(name))
        exit()
     # 处理该目录下的一个文件即可
    dataset = nc.Dataset(paths[0])
    data = dataset.variables["time"][:]
    predit_hour = str(data[-1])
    names = [path.split('/')[-1] for path in paths]
    names_new = []
    for name in names:
        l = name.split("_")
        mode_str = l[0]
        time_str = l[1] + "00"
        area_str = l[-1].split(".")[0]
        new_name = "MSP2_PMSC_AIWSRPF_" + mode_str + "SP1_L88_" + \
            area_str + "_" + time_str + "_00000-0{0}00.nc".format(predit_hour)
        names_new.append(new_name)
    dataset.close()
    return names_new


def get_data(name):
    """获取dem或者降水等数据

    return: 因为经过了reshape, 如果一个文件夹下有10个.nc文件, 每个文件16时次, 最终返回(160, 1701, 2701)
            如果是dem数据, 返回的是(1, 1701, 2701)

            对于新的数据 湿度是 (1, 41, 421, 481), 如果当前文件夹下有10个nc文件, 最终返回的数据shape=(10*41, 421, 481)

    """
    paths = prepare_data(name)
    paths = sorted(paths)
    # print(paths)
    if sys.platform == 'win32':
        splitFlag = '\\'
    elif sys.platform == 'linux':
        splitFlag = '/'
    else:
        print('不支持MACOS')

    category = paths[0].split(splitFlag)[-2]
    if not category.isdigit():
        paths = divide_with_date(paths)
    d = []
    d1 = []
    if not category.isdigit():
        for pathDuo in paths:
            #print(path,' : ',progress(path).shape)
            label = progress(pathDuo[0], category, index=0)  # isobaric
            groundData = progress(pathDuo[1], category, index=1)  # ground
            shap = label.shape
            input = np.zeros((shap[0], shap[1] + 1, shap[2], shap[3]))

            input[:, 0:shap[1], :, :] = label
            input[:, shap[1], :, :] = groundData
            input = np.where(np.isnan(input), 0, input)
            input = np.where(input > 9999, 0, input)
            d.append(input)
            d1.append(label)
    else:
        for path in paths:
            d.append(progress(path, category='dem'))

    d = np.array(d)
    d1 = np.array(d1)
    #print('d.shape: ',d.shape)
    if category.isdigit():
        # dem
        d = np.array(d)
        d = d.reshape(-1, d.shape[-2], d.shape[-1])
        return d
    else:
        # (n, 43, w, j) ---> (n, w, j, 43)
        d = np.array(d)
        d1 = np.array(d1)
        d = d.reshape(-1, d.shape[-2], d.shape[-1], d.shape[-3])
        d1 = d1.reshape(-1, d1.shape[-2], d1.shape[-1], d1.shape[-3])
    #print('d.reshape: ',d.shape)
        return d, d1


def divide_with_date(paths):
    """文件夹下同一个日期包含了地面和高层的数据。两个一组组合。
        return: List[tuple(isobaric, ground)]
    """
    res_paths = []
    while len(paths) > 0:
        tmpTuple = []
        tmpTuple.append(paths[0])
        paths.pop(0)
        for idx, path in enumerate(paths):
            if tmpTuple[0].split('_')[-2] == path.split('_')[-2]:
                tmpTuple.append(path)
                paths.pop(idx)
                tmpTuple.sort()
                res_paths.append(tmpTuple)
    return res_paths


def progress(path, category, index=-1):
    '''处理每个路径下的文件，得到每个文件的降水或地形数据,其中地形和降水标签需要缩放
    param: path: 单独的文件名
         : category: 温度temperature/湿度humidity/风windV, windU, windW/降水precipitation等


        目前将风的三个维度单独考虑, 每次只取其中一个维度. 不将3个共同考虑.
    '''
    # 获取数据
    #type = path.split('/')[-2]
    # print(type)
    data = nc.Dataset(path)
    categoryDict = {
        'temperature': ("Temperature_isobaric", "Temperature_height_above_ground"),
        'humidity': ("Relative_humidity_isobaric", "Relative_humidity_height_above_ground"),
        'windu': ("u-component_of_wind_isobaric", "u-component_of_wind_height_above_ground"),
        'windv': ("v-component_of_wind_isobaric", "v-component_of_wind_height_above_ground"),
        'windW': "Vertical_velocity_pressure_isobaric"
    }

    '''分类型处理'''
    # 地形, 单独成文件夹
    if category == 'dem':
        # if "dem" in path:
        d = data.variables['dem'][:]
    # 降水/温度/湿度/风等 输入
    else:
        dimension = categoryDict[category][index]
        d = data.variables[dimension][:]  # 提取高层数据/地面数据

    data.close()

    return d


if __name__ == '__main__':
    # paths = prepare_data(os.getcwd())
    # paths = sorted(paths)
    # print(paths)

    # # getIsobaricData Demo
    # res = progress(path=paths[0], category='humidity')
    # res1 = progress(path=paths[1], category='humidity')
    # d = []
    # d.append(res)
    # d.append(res1)
    # d = np.array(d)
    # d = d.reshape(-1, d.shape[-2], d.shape[-1])

    # print(d.shape)
    # end getIsobaricData

    # dataset = nc.Dataset(paths[0])
    # data = dataset.variables["time"][:]
    # predit_hour = str(data[-1])
    # names = [path.split('/')[-1] for path in paths]
    # names_new = []
    # for name in names:
    #     l = name.split("_")
    #     mode_str = l[0]
    #     time_str = l[1] + "00"
    #     area_str = l[-1].split(".")[0]
    #     new_name = "MSP2_PMSC_AIWSRPF_" + mode_str + "SP1_L88_" + \
    #         area_str + "_" + time_str + "_00000-0{0}00.nc".format(predit_hour)
    #     names_new.append(new_name)
    # print(names_new)

    # 测试divide_with_date函数
    paths = [r'C:\Users\Kicc\Desktop\3DDecline\label\3\humidity\LAPS_2016060100_RI.nc',
             r'C:\Users\Kicc\Desktop\3DDecline\label\3\humidity\LAPS_2016060100_RS.nc',
             r'C:\Users\Kicc\Desktop\3DDecline\label\3\humidity\LAPS_2016060101_RS.nc',
             r'C:\Users\Kicc\Desktop\3DDecline\label\3\humidity\LAPS_2016060101_RI.nc',
             r'C:\Users\Kicc\Desktop\3DDecline\label\3\humidity\LAPS_2016060102_RS.nc',
             r'C:\Users\Kicc\Desktop\3DDecline\label\3\humidity\LAPS_2016060102_RI.nc']
    res = divide_with_date(paths)
    print(res)

    input_, label_ = get_data(
        name=r'C:\Users\Kicc\Desktop\3DDecline\label\3\humidity')
    print(input_.shape)
    print(label_.shape)
