from utils import (
    input_setup,
    imsave,
    merge
)
import sys
sys.path.append("..")
from read_nc import *
from write_nc import *
import time
import os
import matplotlib.pyplot as plt


import numpy as np
import tensorflow as tf
import sys


class SRCNN(object):

    def __init__(self,
                 sess,
                 input_size_w,
                 image_size_w,
                 label_size_w,
                 input_size_j,
                 image_size_j,
                 label_size_j,
                 batch_size,
                 patch_size,
                 patch_size_l,
                 c_dim=41,
                 checkpoint_dir=None,
                 is_train=True):

        self.sess = sess
        self.is_grayscale = (c_dim == 1)
        self.input_size_w = input_size_w
        self.image_size_w = image_size_w
        self.label_size_w = label_size_w
        self.input_size_j = input_size_j
        self.image_size_j = image_size_j
        self.label_size_j = label_size_j
        self.batch_size = batch_size
        self.patch_size = patch_size
        self.patch_size_l = patch_size_l
        self.c_dim = c_dim

        self.checkpoint_dir = checkpoint_dir
        self.is_train = is_train
        #self.sample_dir = sample_dir
        self.build_model()

    def build_model(self):
        """
        功能: 建立模型, 创建了self.images, self.weights, self.bias 等类成员变量
        输入: 默认, init中接收的FLAGS里的一些参数.
        """
        with tf.name_scope('image_input'):
            self.images = tf.placeholder(
                tf.float32, [None, self.patch_size, self.patch_size, self.c_dim + 2], name='images')

            # tf.placeholder(type, shape, name)
            # shape = [None, 长, 宽, 通道], None表示个数不定, 每个图的大小是patch_size_l*patch_size_l*c_dim
            self.labels = tf.placeholder(
                tf.float32, [None, self.patch_size_l, self.patch_size_l, self.c_dim], name='labels')

        with tf.name_scope('weights'):
            # 参数依次是用一个list表示产出的Tensor的形状[卷积核的高度，卷积核的宽度，图像通道数，卷积核个数], 标准差和name
            # 上一个卷积核个数就是下一个通道数
            self.weights = {
                # 'w1': tf.Variable(tf.random_normal([9, 9, 2, 64], stddev=1e-3), name='w1'),
                # 这里可能改成42通道
                'w1': tf.Variable(tf.random_normal([9, 9, 43, 128], stddev=1e-3), name='w1'),
                'w2': tf.Variable(tf.random_normal([1, 1, 128, 64], stddev=1e-3), name='w2'),
                # 'w3': tf.Variable(tf.random_normal([5, 5, 32, 1], stddev=1e-3), name='w3')
                # 这里最后输出41通道， 然后把下边的bias也改一下
                'w3': tf.Variable(tf.random_normal([5, 5, 64, 41], stddev=1e-3), name='w3')
            }

        with tf.name_scope('biases'):
            self.biases = {
                'b1': tf.Variable(tf.zeros([128]), name='b1'),
                'b2': tf.Variable(tf.zeros([64]), name='b2'),
                'b3': tf.Variable(tf.zeros([41]), name='b3')
            }

        # with tf.name_scope('image_out'):
        # 本文件里的model()函数定义了ＳＲＣＮＮ的三层网络结构，具体见model函数
        self.pred = self.model()
        # tf.summary.image('image_output/output',self.pred,1)

        # Loss function (MSE) 欧氏距离损失函数
        with tf.name_scope('loss'):
            self.loss = tf.reduce_mean(tf.square(self.labels - self.pred))

        # 保存和回复本文件里的train()函数的参数
        self.saver = tf.train.Saver()

    # end build model.

    def train(self, config):
        """
        功能: 训练过程
        """
        if config.is_train:
            # 得到了很多张小图train_data.shape=(-1, w, j, 43) train_label.shape=(-1, w, j, 41)
            train_data, train_label = input_setup(config)
        else:
            try:
                nxy, train_data = input_setup(config)
            except Exception as e:
                print("There is a error:", e)
        print(train_data.shape)

        # Stochastic gradient descent with the standard backpropagation
        self.train_op = tf.train.AdamOptimizer(
            config.learning_rate).minimize(self.loss)

        tf.global_variables_initializer().run()

        counter = 0
        start_time = time.time()

        if self.load(self.checkpoint_dir):
            print(" [*] Load SUCCESS")
        else:
            print(" [!] Load failed...")

        if config.is_train:
            # This is train part.
            print("Training...")
            loss_last = 10
            batch_idxs = len(train_data) // config.batch_size

            for ep in range(config.epoch):
                # Run by batch images
                loss_total = 0
                np.random.seed(ep)
                np.random.shuffle(train_data)
                np.random.seed(ep)
                np.random.shuffle(train_label)
                for idx in range(0, batch_idxs):
                    batch_images = train_data[idx *
                                              config.batch_size: (idx + 1) * config.batch_size]

                    batch_labels = train_label[idx *
                                               config.batch_size: (idx + 1) * config.batch_size]

                    counter += 1
                    _, err = self.sess.run([self.train_op, self.loss], feed_dict={
                                           self.images: batch_images, self.labels: batch_labels})

                    if counter % 400 == 0:
                        print("Epoch:[%2d], step:[%2d], time:[%4.2f秒], loss: [%.16f]"
                              % ((ep + 1), counter, time.time() - start_time, err))
                    loss_total += err
                    if counter % 1000 == 0:
                        with open(r'./epoch.log', 'a+') as f:
                            f.write("Mode: %s, Layer_index: %s, Category: %s, Epoch:[%2d], \
                                step:[%2d], time:[%4.2f秒], loss: [%.16f]\n" % (config.mode, config.layer_index, config.category,
                                                                               (ep + 1), counter, time.time() - start_time, err))

                    if (counter % 200 == 0) and (loss_total / batch_idxs < loss_last):
                        #loss_last = err
                        self.save(config.checkpoint_dir, counter)
                    else:
                        pass
                loss_total = loss_total / batch_idxs
                print("Epoch: [%2d], total_loss: [%.12f]" %
                      ((ep + 1), loss_total))
                if loss_total < 0.05:
                    break
                if (loss_last > loss_total):
                    print("----------------- Better ----------------\n")
                else:
                    pass
                loss_last = loss_total

        # This is test part.
        else:
            print("Testing...")
            bg = 0
            en = 0
            names = get_name(config.time_dimen)
            all_dimen = []
            time_dimen = get_time_dimen(config)
            lat, lon = get_lat_lon_dimen(config)
            isobaric = get_isobaric(config)

            all_dimen.append(time_dimen)
            all_dimen.append(lat)
            all_dimen.append(lon)
            all_dimen.append(isobaric)
            # 如果是16时次, hours_len就是16
            # 如果是目前的三维数据， 时次一直是1, 那么一个文件就是一个时次的
            # 之前的数据是多个时次组成一个文件， 因为生成的文件需要对应的。
            hours_len = int(len(time_dimen))

            writedata = []
            print("The pred hours are:{0}".format(str(hours_len)))
            for i in range(len(nxy)):
                # 因为现在时次为1，所以每次遍历对应的是一个nc文件
                if i % hours_len == 0:
                    print('Reconstructing   ' + names[(i // hours_len)] + ':')

                bg = en
                en += nxy[i][0] * nxy[i][1]
                # train_data[bg:en] 刚好凑成一个时次的大图
                # 比如 nxy[i]=[12, 18], 那么连续的12*18就是一张大图

                # result.shape = (12*18, 93, 93, 41)类似这样的,
                result, _ = self.sess.run([self.pred, self.images], feed_dict={
                    self.images: train_data[bg:en]})
                # re就是一个时次的大图
                # re.shape = (93*8, 93*12, 41)类似这样的
                re = merge(result, nxy[i], config)
                re = re.squeeze()
                # 小于等于0的数,变成0; 大于零的数, 不变
                # re = np.maximum(re, 0)

                print('{0:11}/{1}   |'.format(i % hours_len + 1, str(hours_len)) + '██' * (i %
                                                                                           hours_len + 1) + '  ' * (hours_len - 1 - i % hours_len) + '|', end='\r')
                sys.stdout.flush()

                writedata.append(re)
                if (i + 1) % hours_len == 0:
                    # 如果一个.nc文件是16个时次, 那么就是每16个时次, 写入一遍.
                    outnc = [i for i in all_dimen]
                    # print(outnc[0])
                    outnc.append(writedata)
                    writepath = os.path.join(
                        config.sample_dir, names[(i + 1) // hours_len - 1])
                    writepath = os.path.join(os.getcwd(), writepath)
                    writenc(writepath, outnc, config)
                    writedata = []
                    print('{0:11}/{1}   |'.format(i % hours_len + 1, str(hours_len)) + '██' * (i %
                                                                                               hours_len + 1) + '  ' * (hours_len - 1 - i % hours_len) + '|   COMPLETE!')
                    sys.stdout.flush()

    # end train.
    def layer(self, input, weight, bias):
        conv = tf.nn.conv2d(input, weight, strides=[
            1, 1, 1, 1], padding='VALID') + bias
        batch_normal = tf.layers.batch_normalization(
            conv, training=self.is_train)
        # relu = tf.nn.relu(conv)
        return batch_normal

    def model(self):
        conv1 = tf.nn.relu(self.layer(
            self.images, self.weights['w1'], self.biases['b1']))
        conv2 = tf.nn.relu(self.layer(
            conv1, self.weights['w2'], self.biases['b2']))
        conv3 = self.layer(
            conv2, self.weights['w3'], self.biases['b3'])

        return conv3

    # end model

    def save(self, checkpoint_dir, step):
        model_name = "SRCNN.model"
        model_dir = "%s_%s_%s" % (
            "srcnn", self.label_size_w, self.label_size_j)
        checkpoint_dir = os.path.join(checkpoint_dir, model_dir)

        if not os.path.exists(checkpoint_dir):
            os.makedirs(checkpoint_dir)

        self.saver.save(self.sess,
                        os.path.join(checkpoint_dir, model_name),
                        global_step=step)

    # end save.

    def load(self, checkpoint_dir):
        print(" [*] Reading checkpoints...")
        model_dir = "%s_%s_%s" % (
            "srcnn", self.label_size_w, self.label_size_j)
        checkpoint_dir = os.path.join(checkpoint_dir, model_dir)

        ckpt = tf.train.get_checkpoint_state(checkpoint_dir)
        if ckpt and ckpt.model_checkpoint_path:
            # load最新的那个文件
            ckpt_name = os.path.basename(ckpt.model_checkpoint_path)
            self.saver.restore(self.sess, os.path.join(
                checkpoint_dir, ckpt_name))
            return True
        else:
            return False

    # end load.
